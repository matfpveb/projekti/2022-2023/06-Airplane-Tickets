# Project Airplane Tickets

## :memo: Opis projekta
- Airplane Tickets - aplikacija koja korisnicima omogucava rezervaciju letova. Svaki korisnik ima mogucnost da napravi svoj profil, bira period kada zeli da putuje, vidi listu letova sa detaljima o svakom letu u tom periodu i mogucnost rezervacije sedista. Admin ima mogucnost da vidi sve dostupne letove sa detaljima o svakom letu i da dodaje nove letove.
<br><br>

## :movie_camera: Demo snimak projekta 
- link: https://www.youtube.com/watch?v=arjuPws7gHo <br><br>



## :wrench: Preuzimanje i pokretanje :
- 1. U terminalu se pozicionirati u zeljeni direktorijum
- 2. Klonirati repozitorijum komandom: <br>
    `$ git clone https://gitlab.com/matfpveb/projekti/2022-2023/06-Airplane-Tickets.git`<br
    
    
    ### Za pokretanje backenda :
- 1. Pozicionirati se u backend folder<br>
- 2. Pokrenuti komandu za pravljenje virtualnog okruzenja <br>
    `$ python3 -m venv venv`<br>
- 3. Aktivirati virtualno okruzenje <br>
    `$ source venv/bin/activate ` <br>

- 3. Instalirati sve potrebne requirements-e <br>
    `$ pip3 install -r requiremenents.txt`<br>

- 4. Pokrenuti skriptu za inicijalizaciju test podataka <br>
    `$ python3 initialize_data.py `<br>

- 5. Pokrenuti projekat <br>
    `$ python3 manage.py runserver ` <br>

    ### Za pokretanje frontenda :
- 1. Pozicionirati se u frontend folder <br>
- 2. Instalirati sve potrebne pakete <br>
        `$ npm install` <br>

- 3. Pokrenuti frontend komandom <br>
        `$ ng serve` <br>


### Opis dokumenata iz baze

*User*:
- id: Number
- username: String
- password: String
- name: String
- is_superuser: Boolean
- first_name: String
- last_name: String
- phone_number: String

*Airport*:
- id: Number
- name: String
- city: String

*Flight*:
- id: Number
- airport_from: Airport
- airport_where: Airport
- departure_date: Date
- arrival_date: Date

*FlightTicket*:
- id: Number
- flight: Flight
- user: User
- flightClass: String
- direction: String
- seat_number: String

## Developers

- [Iva Čitlučanin, 143/2019](https://gitlab.com/ivacitlucanin)
- [Nikola Borjan, 10/2018](https://gitlab.com/Dzondzi)
- [Marko Bura, 141/2018](https://gitlab.com/markobura)
- [Andrijana Bosiljčić, 87/2019](https://gitlab.com/andrijanab)
