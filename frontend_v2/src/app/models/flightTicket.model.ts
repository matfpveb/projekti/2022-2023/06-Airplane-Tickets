//import { toBase64String } from "@angular/compiler/src/output/source_map";

import { User } from "./user.model";
import { Flight } from "./flight.model";

export enum Direction {
    "OW",
    "RE"
}

export enum ClassFlight {
    "EC",
    "BU"
}

export class FlightTicket {

    constructor(
        public id: number = 1,
        public flight: Flight = new Flight(),
        public user: User = new User(1, "a", "dsa", 'dsad', false, "ads", "das"),
        public flightClass: ClassFlight = ClassFlight.BU,
        public direction: Direction = Direction.OW,
        public seat_number: string = "A1"
    ) { };

}



