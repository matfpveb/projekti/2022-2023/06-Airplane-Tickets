export class User {
    constructor(
        public id: number,
        public username: string,
        public password: string,
        public name: string,
        public is_superuser: boolean,
        public first_name: string,
        public last_name: string
    ) { }
}