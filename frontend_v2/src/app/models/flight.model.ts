import { Airport } from "./airport.model";

export class Flight {
    constructor(
        public id: number = 1,
        public airport_from: Airport = new Airport(1, "A", "A"),
        public airport_where: Airport = new Airport(1, "A", "A"),
        public departure_date: Date = new Date(),
        public arrival_date: Date = new Date(),
        public taken_seats: String[] = []
    ) { }
}
