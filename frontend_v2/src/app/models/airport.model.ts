
export class Airport {
    constructor(
        public id: number,
        public name: string,
        public city: string
    ) { }
}