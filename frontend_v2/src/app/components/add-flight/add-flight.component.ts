import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Subscription, forkJoin } from "rxjs";
import { Router } from '@angular/router';
import { Airport } from 'src/app/models/airport.model';
import { FlightListService } from 'src/app/services/flight-list.service';


@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.css']
})
export class AddFlightComponent implements OnInit {

  flightForm: FormGroup;
  activeSubscriptions: Subscription[] = [];
  airports: Airport[] = [];

  constructor(private formBuilder: FormBuilder, private flightService: FlightListService, private router: Router) {
    const departure_date = new Date()
    departure_date.setHours(departure_date.getHours() + 1)
    const arrival_date = new Date()
    arrival_date.setHours(arrival_date.getHours() + 1)

    this.flightForm = this.formBuilder.group({
      airport_from: [''],
      departure_date: departure_date.toISOString().substring(0, 16),
      airport_where: [''],
      arrival_date: arrival_date.toISOString().substring(0, 16)
    });

    this.activeSubscriptions.push(this.flightService.getAirports().subscribe(
      (res) => {
        this.airports = res;
      },
      (error) => {
        window.alert("Error fetching airports from database");
      }
    ));

    console.log(this.flightForm.value);
  }

  ngOnInit(): void {

  }

  public ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }


  onAddNewFlight() {
    if (this.flightForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    this.activeSubscriptions.push(this.flightService.postFlight(this.flightForm.value)
      .subscribe((res) => {
        this.router.navigate(['/']);
      },
        (error) => {
          window.alert("Greška: " + error.error);
          console.log(error);
        }
      )
    );
  };

}