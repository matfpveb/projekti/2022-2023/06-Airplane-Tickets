import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscribable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  signinForm: FormGroup;
  activeSubscriptions: Subscription[] = [];

  constructor(public fb: FormBuilder, public authService: AuthService,
    public router: Router) {

    this.signinForm = this.fb.group({
      username: ["", [Validators.required, Validators.pattern(/^[0-9a-zA-Z]{4,}$/)]],
      password: ["", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]],
    });

  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  ngOnInit(): void {
  }

  signin() {
    if (this.signinForm.get('username')?.invalid && this.signinForm.get('password')?.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    if (this.signinForm.get('username')?.invalid) {
      window.alert("Username is not valid!");
      return;
    }

    if (this.signinForm.get('password')?.invalid) {
      window.alert("Password is not valid!");
      return;
    }

    //poslati podatke serveru
    this.activeSubscriptions.push(
      this.authService.login(this.signinForm).subscribe((res: any) => {
        if (!res.user.is_superuser) {
          console.log(res.user.is_superuser);
          this.router.navigate(['/']);
        } else {
          console.log(res.user.is_superuser);
          this.router.navigate(['/']);
        }
      },
        (error) => {
          window.alert("Greška: " + error.error);
          console.log(error);
        })
    )
    this.signinForm.reset({ username: '', email: '', password: '' });
  }



}