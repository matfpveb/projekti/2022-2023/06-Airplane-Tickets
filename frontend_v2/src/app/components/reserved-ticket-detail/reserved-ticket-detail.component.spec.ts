import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservedTicketDetailComponent } from './reserved-ticket-detail.component';

describe('ReservedTicketDetailComponent', () => {
  let component: ReservedTicketDetailComponent;
  let fixture: ComponentFixture<ReservedTicketDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservedTicketDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservedTicketDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
