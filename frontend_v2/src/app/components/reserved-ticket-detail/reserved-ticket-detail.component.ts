import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FlightTicket } from 'src/app/models/flightTicket.model';
import { FlightTicketsService } from 'src/app/services/flight-tickets.service';

@Component({
  selector: 'app-reserved-ticket-detail',
  templateUrl: './reserved-ticket-detail.component.html',
  styleUrls: ['./reserved-ticket-detail.component.css']
})
export class ReservedTicketDetailComponent {
  public selectedFlightTicket: FlightTicket;
  activeSubscriptions: Subscription[] = [];


  constructor(private route: ActivatedRoute, private ticketService: FlightTicketsService) {
    this.selectedFlightTicket = new FlightTicket()
    this.route.paramMap.subscribe(params => {
      const ticketId: number = Number(params.get("ticketId"));
      this.activeSubscriptions.push(
        this.ticketService.getTicket(ticketId).subscribe(
          (res) => {
            this.selectedFlightTicket = res;
          },
          (error) => {
            window.alert("Error fetching from database");
          }
        )
      )
    });
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  ngOnInit() {
  }
}
