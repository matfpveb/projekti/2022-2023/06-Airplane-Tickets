import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { FlightTicket } from 'src/app/models/flightTicket.model';
import { FlightTicketsService } from 'src/app/services/flight-tickets.service';

@Component({
  selector: 'app-reserved-tickets',
  templateUrl: './reserved-tickets.component.html',
  styleUrls: ['./reserved-tickets.component.css']
})
export class ReservedTicketsComponent {
  public flightTickets: FlightTicket[] = [];
  activeSubscriptions: Subscription[] = [];


  constructor(private flightListService: FlightTicketsService) {
    this.activeSubscriptions.push(
      this.flightListService.getTickets().subscribe((res) => {
        this.flightTickets = res;
      },
        (error) => {
          window.alert("Error fetching airports from database");
        })
    )
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

}
