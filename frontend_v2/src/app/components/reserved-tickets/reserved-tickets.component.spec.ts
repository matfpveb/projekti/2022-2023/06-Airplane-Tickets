import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservedTicketsComponent } from './reserved-tickets.component';

describe('ReservedTicketsComponent', () => {
  let component: ReservedTicketsComponent;
  let fixture: ComponentFixture<ReservedTicketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservedTicketsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservedTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
