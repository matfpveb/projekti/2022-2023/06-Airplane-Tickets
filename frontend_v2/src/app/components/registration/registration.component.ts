import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  registerSub: Subscription;
  activeSubscriptions: Subscription[] = [];

  constructor(private authService: AuthService, private router: Router) {
    this.registerForm = new FormGroup({
      first_name: new FormControl("", [Validators.required]),
      last_name: new FormControl("", [Validators.required]),
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
    })

    this.registerSub = new Subscription()
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  ngOnInit(): void {

  }


  public register(): void {
    if (this.registerForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const formData = this.registerForm.value;

    this.activeSubscriptions.push(
      this.authService.registerUser(formData['first_name'], formData['last_name'], formData['username'], formData['password'])
        .subscribe((res) => {
          alert("Account successfully made, you can login now!")
          this.router.navigate(['/login']);
        },
          (error) => {
            window.alert("Greška: " + error.error);
            console.log(error);
          }
        )
    )
  }

}

