import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FlightListService } from 'src/app/services/flight-list.service';

@Component({
  selector: 'app-seat-selector',
  templateUrl: './seat-selector.component.html',
  styleUrls: ['./seat-selector.component.css']
})
export class SeatSelectorComponent {
  public taken_seats: String[] = [];
  activeSubscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private flightService: FlightListService) {
    this.route.paramMap.subscribe(params => {
      // const flightId: number = Number(params.get("flightId"));
      const flightId = 1;
      this.activeSubscriptions.push(
        this.flightService.getFlight(flightId).subscribe(
          (res) => {
            this.taken_seats = res["taken_seats"];
            console.log(this.taken_seats)
          },
          (error) => {
            window.alert("Error fetching from database");
          }
        )
      )
    });
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }


}
