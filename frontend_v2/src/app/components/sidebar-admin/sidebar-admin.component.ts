import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidebar-admin',
  templateUrl: './sidebar-admin.component.html',
  styleUrls: ['./sidebar-admin.component.css']
})
export class SidebarAdminComponent {

  constructor(private authService: AuthService, public router: Router) { }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
