import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Flight } from 'src/app/models/flight.model';
import { ApiService } from 'src/app/services/api-factory.service';
import { FlightListService } from 'src/app/services/flight-list.service';

@Component({
  selector: 'app-flight-detail',
  templateUrl: './flight-detail.component.html',
  styleUrls: ['./flight-detail.component.css']
})
export class FlightDetailComponent implements OnInit, OnDestroy {
  public flight: Flight;
  public taken_seats: String[] = [];
  public selected_seat: String = ""

  activeSubscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private flightService: FlightListService, private api: ApiService, private router: Router) {
    this.flight = new Flight()
    this.activeSubscriptions.push(
      this.route.paramMap.subscribe(params => {
        const flightId: number = Number(params.get("flightId"));
        this.flightService.getFlight(flightId).subscribe(
          (res) => {
            this.flight = res;
            this.taken_seats = this.flight.taken_seats
          },
          (error) => {
            window.alert("Error fetching from database");
          }
        )
      })
    );
  }

  numberOfTakenSeats(): string {
    return (this.taken_seats.length).toString()
  }

  private calculate_class(seat: String) {
    if (Number(seat[0]) > 5) {
      return "EC"
    }
    return "BU"
  }
  private calculate_way(seat: String) {
    if (seat[1] == "A" || seat[1] == "B" || seat[1] == "C") {
      return "OW"
    }
    return "RE"
  }

  selectSeat(seat: string, event: Event) {
    const checkbox = (event.target as HTMLInputElement)
    if (this.selected_seat === seat) {
      this.selected_seat = "";
    } else if (this.selected_seat === "") {
      this.selected_seat = seat;
    } else {
      checkbox.checked = false
      alert("You can select only one seat.");
    }
  }

  onSubmit() {
    const data = {
      user: localStorage.getItem("userId"),
      flight: this.flight.id,
      flightClass: "OW",
      direction: "RE",
      seat_number: "A1"
    }
    const klasa = this.calculate_class(this.selected_seat)
    const way = this.calculate_way(this.selected_seat)
    data["flightClass"] = klasa
    data.flightClass = klasa
    data.direction = way
    data.seat_number = this.selected_seat.toString()
    console.log(data)
    if (data.seat_number !== "") {

      this.activeSubscriptions.push(
        this.api.post('flight/tickets/', data).subscribe((res) => {
          alert("You reserved flight!");
          this.router.navigate(['/ticketList']);
        })
      );
    } else {
      alert("You didn't select any seat");
    }

  }

  calculateDuration(): string {
    const arrival = new Date(this.flight.arrival_date.toString())
    const departure = new Date(this.flight.departure_date.toString())
    const duration = (arrival.getTime() - departure.getTime()) / 60000;
    return duration.toString()
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }
}
