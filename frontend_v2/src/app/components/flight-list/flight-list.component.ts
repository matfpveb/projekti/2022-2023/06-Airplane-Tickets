import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Flight } from 'src/app/models/flight.model';
import { FlightListService } from 'src/app/services/flight-list.service';
import { FlightsByDateService } from 'src/app/services/flights-by-date.service';

@Component({
  selector: 'app-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.css']
})
export class FlightListComponent implements OnInit, OnDestroy {
  public flights: Flight[] = [];
  activeSubscriptions: Subscription[] = [];


  constructor(private flightsByDateService: FlightsByDateService) {

  }

  ngOnInit() {
    this.activeSubscriptions.push(
      this.flightsByDateService.currentApprovalStage.subscribe(fl => this.flights = fl)
    );
    if (this.flights.length == 0) {
      window.alert("There are no flights for the selected dates");
    }
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

}
