import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Flight } from 'src/app/models/flight.model';
import { ApiService } from 'src/app/services/api-factory.service';
import { FlightListService } from 'src/app/services/flight-list.service';

@Component({
  selector: 'app-flight-detail-admin',
  templateUrl: './flight-detail-admin.component.html',
  styleUrls: ['./flight-detail-admin.component.css']
})
export class FlightDetailAdminComponent implements OnInit, OnDestroy {
  public flight: Flight;
  public taken_seats: String[] = [];
  activeSubscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private flightService: FlightListService, private api: ApiService) {
    this.flight = new Flight()
    this.route.paramMap.subscribe(params => {
      const flightId: number = Number(params.get("flightId"));
      this.activeSubscriptions.push(
        this.flightService.getFlight(flightId).subscribe(
          (res) => {
            this.flight = res;
            this.taken_seats = this.flight.taken_seats
          },
          (error) => {
            window.alert("Error fetching from database");
          }
        )
      )
    });
  }

  numberOfTakenSeats(): string {
    return (this.taken_seats.length).toString()
  }

  calculateDuration(): string {
    const arrival = new Date(this.flight.arrival_date.toString())
    const departure = new Date(this.flight.departure_date.toString())
    const duration = (arrival.getTime() - departure.getTime()) / 60000;
    return duration.toString()
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  ngOnInit() {
  }
}
