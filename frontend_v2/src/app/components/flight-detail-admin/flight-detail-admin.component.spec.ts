import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightDetailAdminComponent } from './flight-detail-admin.component';

describe('FlightDetailAdminComponent', () => {
  let component: FlightDetailAdminComponent;
  let fixture: ComponentFixture<FlightDetailAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightDetailAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FlightDetailAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
