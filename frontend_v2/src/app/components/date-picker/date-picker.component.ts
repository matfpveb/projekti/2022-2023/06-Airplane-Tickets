import { Component, OnDestroy, OnInit } from '@angular/core';
import { CalendarView } from 'angular-calendar';
import { CalendarEvent } from 'calendar-utils';
import { Router } from '@angular/router';
import { Flight } from 'src/app/models/flight.model';
import { FlightsByDateService } from 'src/app/services/flights-by-date.service';
import { ReservationsService } from 'src/app/services/reservation.service';
import { startOfDay, endOfDay } from 'date-fns';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit, OnDestroy {
  viewDate: Date = new Date();
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;


  events: CalendarEvent[] = [];
  counter: number = 0;
  startDate: Date = new Date();
  endDate: Date = new Date();

  setDateBtnDisabled: boolean = false;

  flightsByDate: Flight[] = [];
  activeSubscriptions: Subscription[] = [];


  constructor(
    public reservationService: ReservationsService, public router: Router, public flightByDateService: FlightsByDateService
  ) { }

  ngOnInit() {
    this.activeSubscriptions.push(
      this.flightByDateService.currentApprovalStage.subscribe(fl => this.flightsByDate = fl)
    );
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  dayClicked({ date }: { date: Date }): void {
    if (date < new Date()) {
      window.alert("You can`t choose a date before today");
      return;
    }

    this.counter += 1;

    if (this.counter == 1) {
      this.startDate = date;
    }
    if (this.counter == 2) {
      this.endDate = date;
      if (this.endDate < this.startDate) {
        window.alert("You can't choose a date before " + this.startDate + " for arrival time");
        this.counter = 0;
      }
      else {
        this.events = [
          ...this.events,
          {
            start: startOfDay(this.startDate),
            end: endOfDay(this.endDate),
            title: 'Booked',
          }
        ]
        this.setDateBtnDisabled = true;
        return;
      }
    }

  }

  createJsonObject(sDate: Date, eDate: Date) {
    var stDate = sDate.toISOString().substring(0, 10);
    var enDate = eDate.toISOString().substring(0, 10);
    var jsonData = { fromDate: stDate, toDate: enDate }
    console.log("JSON from pick-flightc" + JSON.stringify(jsonData))
    return JSON.stringify(jsonData)
  }

  onGetFlihts() {
    this.activeSubscriptions.push(
      this.reservationService.getFlightsByDate(this.createJsonObject(this.startDate, this.endDate))
        .subscribe((res) => {
          this.flightsByDate = res;
          if (this.flightsByDate.length === 0) {
            alert("There are no flights for selected dates")
            this.router.navigate(['/']);
            return;
          }
          this.router.navigate(['/flightUser']);
          this.flightByDateService.updateApproval(this.flightsByDate);
        },
          (error) => {
            window.alert("Error fetching flights from database");
          }
        )
    );
  }


}

