import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightListAdminComponent } from './flight-list-admin.component';

describe('FlightListAdminComponent', () => {
  let component: FlightListAdminComponent;
  let fixture: ComponentFixture<FlightListAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightListAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FlightListAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
