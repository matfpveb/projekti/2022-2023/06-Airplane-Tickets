import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Flight } from 'src/app/models/flight.model';
import { FlightListService } from 'src/app/services/flight-list.service';

@Component({
  selector: 'app-flight-list-admin',
  templateUrl: './flight-list-admin.component.html',
  styleUrls: ['./flight-list-admin.component.css']
})
export class FlightListAdminComponent implements OnInit, OnDestroy {
  public flights: Flight[] = [];
  activeSubscriptions: Subscription[] = [];


  constructor(private flightListService: FlightListService) {
    this.activeSubscriptions.push(
      this.flightListService.getFlights().subscribe((res) => {
        this.flights = res;
      })
    );
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

}
