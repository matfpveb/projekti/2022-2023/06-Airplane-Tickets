import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FlightListComponent } from './components/flight-list/flight-list.component';
import { FlightDetailComponent } from './components/flight-detail/flight-detail.component';
import { SidebarAdminComponent } from './components/sidebar-admin/sidebar-admin.component';
import { ApiService } from './services/api-factory.service';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddFlightComponent } from './components/add-flight/add-flight.component';
import { ReservedTicketsComponent } from './components/reserved-tickets/reserved-tickets.component';
import { ReservedTicketDetailComponent } from './components/reserved-ticket-detail/reserved-ticket-detail.component';
import { SeatSelectorComponent } from './components/seat-selector/seat-selector.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { CalendarModule, DateAdapter } from 'angular-calendar'
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FlightListAdminComponent } from './components/flight-list-admin/flight-list-admin.component';
import { FlightDetailAdminComponent } from './components/flight-detail-admin/flight-detail-admin.component';
import { HomePageComponent } from './components/home-page/home-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    NavbarComponent,
    SidebarComponent,
    FlightListComponent,
    FlightDetailComponent,
    SidebarAdminComponent,
    AddFlightComponent,
    ReservedTicketsComponent,
    ReservedTicketDetailComponent,
    SeatSelectorComponent,
    DatePickerComponent,
    FlightListAdminComponent,
    FlightDetailAdminComponent,
    HomePageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
  providers: [ApiService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
