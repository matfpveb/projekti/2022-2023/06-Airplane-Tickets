import { Component } from '@angular/core';
import { ApiService } from './services/api-factory.service';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend_v2';

  constructor(public authService: AuthService, public apiService: ApiService) {
    if (this.authService.isLoggedIn()) {
      this.apiService.setAuthHeaders(this.authService.getToken())
    }
  }
}

