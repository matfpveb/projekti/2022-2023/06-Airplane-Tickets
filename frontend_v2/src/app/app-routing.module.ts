import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddFlightComponent } from './components/add-flight/add-flight.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { FlightDetailAdminComponent } from './components/flight-detail-admin/flight-detail-admin.component';
import { FlightDetailComponent } from './components/flight-detail/flight-detail.component';
import { FlightListAdminComponent } from './components/flight-list-admin/flight-list-admin.component';
import { FlightListComponent } from './components/flight-list/flight-list.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ReservedTicketDetailComponent } from './components/reserved-ticket-detail/reserved-ticket-detail.component';
import { ReservedTicketsComponent } from './components/reserved-tickets/reserved-tickets.component';
import { SeatSelectorComponent } from './components/seat-selector/seat-selector.component';
import { AuthGuard, AuthAdminGuard, AuthUserGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: HomePageComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'flightList', component: FlightListAdminComponent, canActivate: [AuthAdminGuard] },
  { path: 'flightUser', component: FlightListComponent, canActivate: [AuthUserGuard] },
  { path: 'flightList/:flightId', component: FlightDetailAdminComponent, canActivate: [AuthAdminGuard] },
  { path: 'flightUser/:flightId', component: FlightDetailComponent, canActivate: [AuthUserGuard] },
  { path: 'ticketList', component: ReservedTicketsComponent, canActivate: [AuthGuard] },
  { path: 'ticketList/:ticketId', component: ReservedTicketDetailComponent, canActivate: [AuthGuard] },
  { path: 'add-flight', component: AddFlightComponent, canActivate: [AuthGuard] },
  { path: 'seat-selection', component: SeatSelectorComponent, canActivate: [AuthGuard] },
  { path: 'flightsByDate', component: DatePickerComponent, canActivate: [AuthGuard] },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
