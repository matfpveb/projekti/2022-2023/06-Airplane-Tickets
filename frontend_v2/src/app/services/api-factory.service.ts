import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly baseUrl: string = 'http://127.0.0.1:8000/';
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private authToken = "";
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') }

  constructor(private http: HttpClient) {
  }

  get<T>(path: string): Observable<T> {
    const h = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.authToken.toString())
    return this.http.get<T>(`${this.baseUrl}${path}`, { headers: h });
  }

  post<T>(path: string, data: any): Observable<T> {
    const h = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.authToken.toString())
    return this.http.post<T>(`${this.baseUrl}${path}`, data, { headers: h });
  }

  put<T>(path: string, data: any): Observable<T> {
    const h = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.authToken.toString())
    return this.http.put<T>(`${this.baseUrl}${path}`, data, { headers: h });
  }

  delete<T>(path: string): Observable<T> {
    const h = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.authToken.toString())
    return this.http.delete<T>(`${this.baseUrl}${path}`, { headers: h });
  }

  patch<T>(path: string, data: any): Observable<T> {
    const h = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.authToken.toString())
    return this.http.patch<T>(`${this.baseUrl}${path}`, data, { headers: h });
  }

  setAuthHeaders(token: string) {
    this.headers.set('Authorization', token);
    this.authToken = token;
    console.log("Setting authorization headers")
    this.options = { headers: this.headers }
  }

  unsetAuthHeaders() {
    this.authToken = ""
  }

}
