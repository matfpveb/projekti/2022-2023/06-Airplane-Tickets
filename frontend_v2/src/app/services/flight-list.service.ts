import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Airport } from '../models/airport.model';
import { Flight } from '../models/flight.model';
import { ApiService } from './api-factory.service';

@Injectable({
  providedIn: 'root'
})
export class FlightListService {

  constructor(private api: ApiService) {
  }

  public getFlights() {
    return this.api.get<Flight[]>('flight/flights/')
  }

  public getFlight(id: number) {
    return this.api.get<Flight>('flight/flights/' + id + '/')
  }

  public getAirports() {
    return this.api.get<Airport[]>('flight/airports/');
  }

  public postFlight(data: object): Observable<Flight> {
    return this.api.post<Flight>('flight/flights/', data);
  }

}
