import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Flight } from '../models/flight.model';

@Injectable({
  providedIn: 'root'
})
export class FlightsByDateService {
  flightsByDate: Flight[] = [];
  private approvalStage = new BehaviorSubject(this.flightsByDate);
  currentApprovalStage = this.approvalStage.asObservable();

  constructor() {

  }

  updateApproval(flByDate: Flight[]) {
    this.approvalStage.next(flByDate);
  }
}
