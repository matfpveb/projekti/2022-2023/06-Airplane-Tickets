import { TestBed } from '@angular/core/testing';

import { FlightsByDateService } from './flights-by-date.service';

describe('FlightsByDateService', () => {
  let service: FlightsByDateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlightsByDateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
