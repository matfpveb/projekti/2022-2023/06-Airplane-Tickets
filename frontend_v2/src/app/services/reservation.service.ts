import { Injectable } from '@angular/core';
import { Flight } from '../models/flight.model';
import { ApiService } from './api-factory.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {
  private token: any = this.authService.getToken();

  constructor(private api: ApiService, private authService: AuthService) {

  }

  getFlightsByDate(object: any) {
    return this.api.post<Flight[]>("flight/flightsDate/", object);
  }

}
