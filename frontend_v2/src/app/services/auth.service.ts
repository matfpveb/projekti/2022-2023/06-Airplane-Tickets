import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { User } from '../models/user.model';
import { ApiService } from './api-factory.service';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = false;
  private token: any;
  private user: User | null;

  constructor(private api: ApiService, private http: HttpClient) {
    this.loggedIn = !!localStorage.getItem('jwt');
    this.token = localStorage.getItem('jwt');
    const userItem = localStorage.getItem('user')
    if (userItem != null)
      this.user = JSON.parse(userItem);
    else
      this.user = null

  }

  login(form: FormGroup) {
    const username = form.value.username
    const password = form.value.password
    return this.api.post<any>('user/login/', { username, password })
      .pipe(tap(res => {
        localStorage.setItem('jwt', res.jwt);
        this.api.setAuthHeaders(res.jwt.toString());
        localStorage.setItem('user', JSON.stringify(res.user));
        localStorage.setItem('userId', res.user.id)
        this.loggedIn = true;
        this.token = res.jwt;
        this.user = res.user;
      }));
  }

  registerUser(first_name: string, last_name: string, username: string, password: string) {
    return this.api.post<any>('user/register/', { first_name, last_name, username, password });
  }

  logout() {
    localStorage.removeItem('jwt');
    localStorage.removeItem('user');
    localStorage.removeItem('userId');
    this.api.unsetAuthHeaders();
    this.loggedIn = false;
    this.token = null;
    this.user = null;
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  isSuperUser() {
    return this.user ? this.user.is_superuser : false;
  }

  getUser() {
    return this.user;
  }

  getToken() {
    return this.token;
  }
}
