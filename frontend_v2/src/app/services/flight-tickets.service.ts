import { Injectable } from '@angular/core';
import { FlightTicket } from '../models/flightTicket.model';
import { ApiService } from './api-factory.service';

@Injectable({
  providedIn: 'root'
})
export class FlightTicketsService {

  constructor(private api: ApiService) {
  }

  public getTickets() {
    return this.api.get<FlightTicket[]>('flight/tickets/')
  }

  public getTicket(id: number) {
    return this.api.get<FlightTicket>('flight/tickets/' + id + '/')
  }

}
