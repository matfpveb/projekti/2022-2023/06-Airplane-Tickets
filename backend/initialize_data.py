import datetime
import json
import os
import random
import shutil
import subprocess
import sys
import django

from termcolor import colored


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "source.settings")
django.setup()

from flight.models import Airport, Flight, FlightTicket
from user.models import User


applications = ["flight", "user"]


def drop_database():
    print("Do you want to delete previous database and migrations before starting?[y/n]")
    response = input()
    if response.lower() == 'y':
        db_path = os.path.join(os.getcwd(), 'db.sqlite3')
        if os.path.exists(db_path):
            os.remove(db_path)
            print(colored(str("db.sqlite3 file deleted"), "green"))

        for application in applications:
            application_path = os.path.join(os.getcwd(), application, 'migrations')
            if os.path.exists(application_path):
                shutil.rmtree(application_path)
                print(colored(str(f"{application}/migrations folder deleted"), "green"))


def airports_population():
    print(colored("Populating airports...", "blue"))
    filename = f"resources/airports.json"
    with open(filename) as f:
        json_data = json.load(f)
    for el in json_data:
        Airport.objects.create(**el)
    print(colored("Airports population successfully", "green"))


def flights_populate():
    print(colored("Populating flights...", "blue"))
    filename = f"resources/flights.json"
    with open(filename) as f:
        json_data = json.load(f)
    for el in json_data:
        airport_from = Airport.objects.get(id=el["airport_from"])
        airport_where = Airport.objects.get(id=el["airport_where"])
        departure_date = datetime.datetime.now() + \
                         datetime.timedelta(days=random.randint(0, 25)) +\
                         datetime.timedelta(hours=random.randint(0, 24)) +\
                         datetime.timedelta(minutes=random.randint(0, 60))
        arrival_date = departure_date + datetime.timedelta(minutes=el["duration"])
        Flight.objects.create(airport_from=airport_from,
                              airport_where=airport_where,
                              departure_date=departure_date,
                              arrival_date=arrival_date)
    print(colored("Flights population successfully", "green"))


def user_population():
    print(colored("Populating users...", "blue"))
    filename = f"resources/users.json"
    with open(filename) as f:
        json_data = json.load(f)
    for el in json_data:
        password = el["password"]
        user = User.objects.create(**el)
        user.set_password(password)
        user.save()
    print(colored("Users population successfully", "green"))


def flight_reservation_population():
    print(colored("Populating reservations...", "blue"))
    filename = f"resources/reservations.json"
    with open(filename) as f:
        json_data = json.load(f)
    for el in json_data:
        flight_id = el.pop("flight")
        flight = Flight.objects.get(id=flight_id)
        user_id = el.pop("user")
        user = User.objects.get(id=user_id)
        reservation = FlightTicket.objects.create(**el, flight=flight, user=user)
    print(colored("Flight reservations population successfully", "green"))


if __name__ == "__main__":
    try:
        drop_database()
    except Exception as e:
        print(colored("Dropping database failed", 'red'))
        print(colored(str(e), "red"))
        exit()

    try:
        print(colored("Migrations: ", 'blue'))
        make_migrations = subprocess.call([sys.executable, 'manage.py', 'makemigrations'] + applications)
        migrate = subprocess.call([sys.executable, 'manage.py', 'migrate'])
    except Exception as e:
        print(colored("Migrations failed", 'red'))
        print(colored(str(e), "red"))
        exit()

    try:
        airports_population()
        user_population()
        flights_populate()
        flight_reservation_population()
    except Exception as e:
        print(colored(str(e), "red"))
