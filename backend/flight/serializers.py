import sys
sys.path.append("..")
from rest_framework import serializers

from .models import FlightTicket, Flight, Airport
from user.serializers import UserSerializer


class AirportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Airport
        fields = "__all__"


class FlightPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Flight
        fields = "__all__"


class FlightSerializer(serializers.ModelSerializer):
    taken_seats = serializers.ReadOnlyField()
    airport_from = AirportSerializer(read_only=True)
    airport_where = AirportSerializer(read_only=True)

    class Meta:
        model = Flight
        fields = "__all__"


class FlightTicketPOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlightTicket
        fields = "__all__"


class FlightTicketGETSerializer(serializers.ModelSerializer):
    flight = FlightSerializer(read_only=True)
    user = UserSerializer(read_only=True)
    direction = serializers.ReadOnlyField()
    flightClass = serializers.ReadOnlyField()
    seat_number = serializers.ReadOnlyField()

    class Meta:
        model = FlightTicket
        fields = "__all__"
