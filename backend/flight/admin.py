from django.contrib import admin

from .models import FlightTicket, Flight, Airport

admin.site.register(FlightTicket)
admin.site.register(Flight)
admin.site.register(Airport)
