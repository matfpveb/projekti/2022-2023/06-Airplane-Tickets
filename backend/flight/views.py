import datetime

from rest_framework import status
from rest_framework.generics import GenericAPIView, get_object_or_404, RetrieveAPIView

from .models import FlightTicket, Flight, Airport
from .serializers import FlightSerializer, FlightTicketGETSerializer, AirportSerializer, \
    FlightPOSTSerializer, FlightTicketPOSTSerializer
from rest_framework.response import Response


class FlightDetailRetrieveAPIView(RetrieveAPIView):
    queryset = Flight.objects.all()
    serializer_class = FlightSerializer

    def get(self, request, *args, **kwargs):
        instance = get_object_or_404(Flight, id=kwargs.get("id"))
        serializer = self.get_serializer_class()(instance)
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class FlightTicketDetailRetrieveAPIView(RetrieveAPIView):
    queryset = FlightTicket.objects.all()
    serializer_class = FlightTicketGETSerializer

    def get(self, request, *args, **kwargs):
        instance = get_object_or_404(FlightTicket, id=kwargs.get("id"))
        serializer = self.get_serializer_class()(instance)
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class FlightTicketGenericAPIView(GenericAPIView):
    queryset = FlightTicket.objects.all()
    serializer_class = FlightTicketGETSerializer

    def get_serializer_class(self):
        if self.request.method == "GET":
            return FlightTicketGETSerializer
        else:
            return FlightTicketPOSTSerializer

    def get_queryset(self):
        if not self.request.user.is_superuser:
            return self.queryset.filter(user=self.request.user)
        return self.queryset.all()

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        serializer = self.get_serializer_class()(qs, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

    def delete(self, request, *args, **kwargs):
        instance = get_object_or_404(FlightTicket, id=request.data["id"])
        instance.delete()
        return Response(status=status.HTTP_200_OK, data={"message": "success"})


class FlightDateGet(GenericAPIView):
    queryset = Flight.objects.all()
    serializer_class = FlightSerializer

    def post(self, request, *args, **kwargs):
        from_date = request.data["fromDate"]
        to_date = request.data["toDate"]
        qs = Flight.objects.filter(departure_date__range=[from_date, to_date])
        serializer = self.get_serializer_class()(qs, many=True)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class FlightGenericAPIView(GenericAPIView):
    queryset = Flight.objects.all()
    serializer_class = FlightSerializer

    def get_serializer_class(self):
        if self.request.method == "GET":
            return FlightSerializer
        else:
            return FlightPOSTSerializer

    def get_queryset(self):
        if not self.request.user.is_superuser:
            return self.queryset.filter(departure_date__gt=datetime.datetime.now())
        return self.queryset.all()

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        serializer = self.get_serializer_class()(qs, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

    def delete(self, request, *args, **kwargs):
        instance = get_object_or_404(Flight, id=request.data["id"])
        instance.delete()
        return Response(status=status.HTTP_200_OK, data={"message": "success"})


class AirportRetrieveAPIView(RetrieveAPIView):
    queryset = Airport.objects.all()
    serializer_class = AirportSerializer

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        serializer = self.get_serializer_class()(qs, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)