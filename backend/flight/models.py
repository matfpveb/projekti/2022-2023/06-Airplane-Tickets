from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _


User = get_user_model()


class FlightClass(models.TextChoices):
    ECONOMY = 'EC', _('Economy')
    BUSINESS = 'BU', _('Business')


class Direction(models.TextChoices):
    ONE_WAY = 'OW', _('One way')
    RETURN = 'RE', _('Return flight')


class Airport(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return f"{self.name} - {self.city}"


class Flight(models.Model):
    airport_from = models.ForeignKey(to=Airport, on_delete=models.CASCADE, related_name="airport_from")
    airport_where = models.ForeignKey(to=Airport, on_delete=models.CASCADE, related_name="airport_where")
    departure_date = models.DateTimeField(null=True, blank=True)
    arrival_date = models.DateTimeField(null=True, blank=True)

    @property
    def taken_seats(self):
        return [ticket.seat_number for ticket in FlightTicket.objects.filter(flight=self)]

    def __str__(self):
        return f"{self.airport_from} - {self.airport_where} - {self.departure_date.strftime('%Y-%m-%d %H:%M')}"

    class Meta:
        ordering = ["departure_date"]


class FlightTicket(models.Model):
    flight = models.ForeignKey(Flight, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    flightClass = models.CharField(max_length=2, choices=FlightClass.choices)
    direction = models.CharField(max_length=2, choices=Direction.choices, default=Direction.ONE_WAY)
    seat_number = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.user.full_name}'s flight {self.flight}"

    class Meta:
        ordering = ["flight"]
