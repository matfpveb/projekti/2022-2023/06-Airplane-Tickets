from django.urls import path

from .views import FlightTicketGenericAPIView, FlightGenericAPIView, AirportRetrieveAPIView, FlightDateGet, \
    FlightDetailRetrieveAPIView, FlightTicketDetailRetrieveAPIView

urlpatterns = [
    path('tickets/', FlightTicketGenericAPIView.as_view(), name="tickets"),
    path('tickets/<int:id>/', FlightTicketDetailRetrieveAPIView.as_view(), name="ticketsDetail"),
    path('flights/', FlightGenericAPIView.as_view(), name="flights"),
    path('flights/<int:id>/', FlightDetailRetrieveAPIView.as_view(), name="flightsDetail"),
    path('flightsDate/', FlightDateGet.as_view(), name="flights_date"),
    path('airports/', AirportRetrieveAPIView.as_view(), name="airports"),

]