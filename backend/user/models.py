from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    phone_number = models.TextField(null=True, blank=True)
    REQUIRED_FIELDS = []

    @property
    def full_name(self):
        return self.get_full_name()
