from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response

from .authenticate import generate_access_token
from .serializers import UserSerializer

User = get_user_model()


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def login(request, *args, **kwargs):
    """
    Login with email and password:
        required fields: [ email: string, password: string]
    """
    username = request.data["username"]
    password = request.data["password"]

    user = User.objects.filter(username=username).first()
    if user is None or not user.check_password(password):
        return Response(status=status.HTTP_401_UNAUTHORIZED, data={"message": "failed", "reason": f"Wrong username or password!"})
    if not user.is_active:
        return Response(status=status.HTTP_401_UNAUTHORIZED, data={"message": "failed", "reason": f"Your account is inactive!"})

    response = Response()
    token = generate_access_token(user)

    serializer = UserSerializer(user)
    response.data = {"message": "success", "jwt": token, "user": serializer.data}

    return response


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
@transaction.atomic
def register(request, *args, **kwargs):
    serializer = UserSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user = serializer.save()
    user.set_password(request.data["password"])
    user.save()
    return Response(status=status.HTTP_201_CREATED, data={"message": "success"})
